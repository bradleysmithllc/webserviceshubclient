package org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.test;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ScopeSection;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ScopeSectionImpl;
import org.bitbucket.bradleysmithllc.webserviceshubclient.regexp.ParameterExpression;
import org.bitbucket.bradleysmithllc.webserviceshubclient.regexp.ScopeExpression;
import org.junit.Assert;
import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;

public class WriterTest
{
	@Test
	public void session()
	{
		Assert.assertEquals("[Global]", loadScope("[Global]").getHeader());
		Assert.assertEquals("[session]", loadScope("[session]").getHeader());
		Assert.assertEquals("[Service:service]", loadScope("[Service:service]").getHeader());
		Assert.assertEquals("[Service:service.ND:node]", loadScope("[Service:service.ND:node]").getHeader());
		Assert.assertEquals("[folder.session]", loadScope("[folder.session]").getHeader());
		Assert.assertEquals("[folder.WF:workflow]", loadScope("[folder.WF:workflow]").getHeader());
		Assert.assertEquals("[folder.WF:workflow.WT:worklet]", loadScope("[folder.WF:workflow.WT:worklet]").getHeader());
		Assert.assertEquals("[folder.WF:workflow.WT:worklet.ST:session]", loadScope("[folder.WF:workflow.WT:worklet.ST:session]").getHeader());
	}

	private ScopeSection loadScope(String s)
	{
		ScopeExpression se = new ScopeExpression(s);

		if (se.matches())
		{
			return new ScopeSectionImpl(se);
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Test
	public void parameter()
	{
		Assert.assertEquals("$A=b", loadParemeter("$A=b").trim());
		Assert.assertEquals("mplt.$A=b", loadParemeter("mplt.$A=b").trim());
	}

	private String loadParemeter(String line)
	{
		ParameterExpression se = new ParameterExpression(line);

		if (se.matches())
		{
			ScopeSection ss = loadScope("[Global]");

			ss.appendParameter(se);

			StringWriter out = new StringWriter();
			PrintWriter swr = new PrintWriter(out);

			ss.getParameters().get(0).write(swr);

			return out.toString();
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}
}