package org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.test;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.junit.Assert;
import org.junit.Test;

public class ParameterFileCompareTest
{
	@Test
	public void compareEmpty()
	{
		Assert.assertEquals(0, new ParameterFile().compareTo(new ParameterFile()));
	}

	@Test
	public void compareEqual()
	{
		ParameterFile parameterFile = new ParameterFile();
		parameterFile.appendSection("[section]").appendParameter("$One", "#One");

		ParameterFile parameterFile1 = new ParameterFile();
		parameterFile1.appendSection("[section]").appendParameter("$One", "#One");

		Assert.assertEquals(0, parameterFile.compareTo(parameterFile1));
	}

	@Test
	public void compareUnequal()
	{
		ParameterFile parameterFile = new ParameterFile();
		parameterFile.appendSection("[section]").appendParameter("$One", "#One");

		ParameterFile parameterFile1 = new ParameterFile();
		parameterFile1.appendSection("[Global]").appendParameter("$One", "#One");

		Assert.assertTrue(parameterFile.compareTo(parameterFile1) != 0);
	}

	@Test
	public void compareUnequalMapplet()
	{
		ParameterFile parameterFile = new ParameterFile();
		parameterFile.appendSection("[section]").appendParameter("$One", "#One");

		ParameterFile parameterFile1 = new ParameterFile();
		parameterFile1.appendSection("[section]").appendParameter("mapplet", "$One", "#One");

		Assert.assertTrue("Files should not match", parameterFile.compareTo(parameterFile1) != 0);
	}
}