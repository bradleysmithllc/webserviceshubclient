package org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.test;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ScopeSection;
import junit.framework.Assert;
import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class PRMTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void testEmpty() throws IOException
	{
		ParameterFile pm = loadFile("");

		Assert.assertEquals(0, pm.getSections().size());
	}

	@Test
	public void testGlobal() throws IOException
	{
		ParameterFile pm = loadFile("[Global]\n" +
				"$A=B");

		Assert.assertEquals(1, pm.getSections().size());

		Assert.assertEquals(ScopeSection.section_type.global, pm.getSections().get(0).getType());
		Assert.assertEquals(1, pm.getSections().get(0).getParameters().size());

		Assert.assertNull(pm.getSections().get(0).getParameters().get(0).getMappletName());
		Assert.assertEquals("$A", pm.getSections().get(0).getParameters().get(0).getParameterName());
		Assert.assertEquals("B", pm.getSections().get(0).getParameters().get(0).getParameterValue());
	}

	private ParameterFile loadFile(String s) throws IOException
	{
		File prm = temporaryFolder.newFile(String.valueOf(System.currentTimeMillis()));

		FileUtils.write(prm, s);

		return new ParameterFile(prm);
	}
}