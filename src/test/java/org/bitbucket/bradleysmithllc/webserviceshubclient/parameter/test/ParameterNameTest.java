package org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.test;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterImpl;
import org.junit.Test;

public class ParameterNameTest
{
	@Test
	public void nameWithoutMapplet()
	{
		ParameterImpl pimpl = new ParameterImpl(null, "a", "b");

		Assert.assertEquals(pimpl.getQualifiedParameterName(), "a");
		Assert.assertEquals(pimpl.getQualifiedParameterName(), pimpl.getParameterName());
		Assert.assertEquals("a=b", pimpl.asString());
	}

	@Test
	public void nameWithMapplet()
	{
		ParameterImpl pimpl = new ParameterImpl("mplt", "a", "b");

		Assert.assertEquals(pimpl.getQualifiedParameterName(), "mplt.a");
		Assert.assertEquals(pimpl.getParameterName(), "a");
		Assert.assertEquals("mplt.a=b", pimpl.asString());
	}
}