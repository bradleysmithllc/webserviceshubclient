package org.bitbucket.bradleysmithllc.webserviceshubclient;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.informatica.wsh.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.java_cl_parser.CapturingShutdownHandler;
import org.bitbucket.bradleysmithllc.java_cl_parser.ShutdownHandler;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

public class GetWorkflowDetailsIntegrationTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	private CapturingShutdownHandler capturingShutdownHandler;

	@Before
	public void setupShutdownHandler()
	{
		capturingShutdownHandler = new CapturingShutdownHandler();
		ShutdownHandler.setInstance(capturingShutdownHandler);
	}

	@Test
	public void workflowExists() throws IOException
	{
		GetWorkflowDetails didet = (GetWorkflowDetails) GetWorkflowDetails.mainWithReturn(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-f",
				"REPOSITORY_UTILITIES",
				"-wkf",
				"wkf_LOAD_NO_DATA",
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL"
		});

		Assert.assertNull(capturingShutdownHandler.getExitCode());

		Assert.assertNotNull(didet);
		Assert.assertNotNull(didet.getWorkflowDetails());
		Assert.assertEquals("REPOSITORY_UTILITIES", didet.getWorkflowDetails().getWorkflowDetails().get(0).getFolderName());
		Assert.assertEquals("wkf_LOAD_NO_DATA", didet.getWorkflowDetails().getWorkflowDetails().get(0).getWorkflowName());
	}

	@Test
	public void workflowNotExists() throws IOException
	{
		GetWorkflowDetails.main(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-f",
				"_BSMITH",
				"-wkf",
				"wkf_LOAD_NO_DATA~~~~~",
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL"
		});

		Assert.assertEquals(-1, capturingShutdownHandler.getExitCode().intValue());
	}
}