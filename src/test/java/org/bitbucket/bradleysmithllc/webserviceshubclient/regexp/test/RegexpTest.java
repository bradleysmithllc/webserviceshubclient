package org.bitbucket.bradleysmithllc.webserviceshubclient.regexp.test;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.bitbucket.bradleysmithllc.webserviceshubclient.regexp.ParameterExpression;
import org.bitbucket.bradleysmithllc.webserviceshubclient.regexp.ScopeExpression;
import org.junit.Assert;
import org.junit.Test;

public class RegexpTest
{
	@Test
	public void testGlobalScope()
	{
		ScopeExpression scopeExpression = new ScopeExpression("[Global]");
		Assert.assertTrue(scopeExpression.matches());
		Assert.assertTrue(scopeExpression.hasGlobal());
		Assert.assertFalse(scopeExpression.hasSession());
		Assert.assertFalse(scopeExpression.hasFolderSession());
		Assert.assertFalse(scopeExpression.hasQualifiedWorkflowSession());

		scopeExpression = new ScopeExpression("[GLOBAL]");
		Assert.assertTrue(scopeExpression.matches());
		Assert.assertTrue(scopeExpression.hasGlobal());
		Assert.assertFalse(scopeExpression.hasSession());
		Assert.assertFalse(scopeExpression.hasFolderSession());
		Assert.assertFalse(scopeExpression.hasQualifiedWorkflowSession());

		scopeExpression = new ScopeExpression("[global]");
		Assert.assertTrue(scopeExpression.matches());
		Assert.assertTrue(scopeExpression.hasGlobal());
		Assert.assertFalse(scopeExpression.hasSession());
		Assert.assertFalse(scopeExpression.hasFolderSession());
		Assert.assertFalse(scopeExpression.hasQualifiedWorkflowSession());
	}

	@Test
	public void testSessionScope()
	{
		ScopeExpression scopeExpression = new ScopeExpression("[session]");
		Assert.assertTrue(scopeExpression.matches());
		Assert.assertTrue(scopeExpression.hasSession());
		Assert.assertFalse(scopeExpression.hasGlobal());
		Assert.assertFalse(scopeExpression.hasFolderSession());
		Assert.assertFalse(scopeExpression.hasQualifiedWorkflowSession());

		Assert.assertEquals("session", scopeExpression.getSession());
	}

	@Test
	public void testFolderSessionScope()
	{
		ScopeExpression scopeExpression = new ScopeExpression("[Folder.session]");
		Assert.assertTrue(scopeExpression.matches());
		Assert.assertTrue(scopeExpression.hasFolderSession());
		Assert.assertFalse(scopeExpression.hasGlobal());
		Assert.assertFalse(scopeExpression.hasSession());
		Assert.assertFalse(scopeExpression.hasQualifiedWorkflowSession());

		Assert.assertEquals("session", scopeExpression.getFolderSessionName());
		Assert.assertEquals("Folder", scopeExpression.getFolderName());
	}

	@Test
	public void testIntegrationServiceScope()
	{
		ScopeExpression scopeExpression = new ScopeExpression("[Service:service]");
		Assert.assertTrue(scopeExpression.matches());
		Assert.assertFalse(scopeExpression.hasFolderSession());
		Assert.assertFalse(scopeExpression.hasGlobal());
		Assert.assertFalse(scopeExpression.hasSession());
		Assert.assertTrue(scopeExpression.hasIntegrationService());
		Assert.assertFalse(scopeExpression.hasNodeName());
		Assert.assertFalse(scopeExpression.hasQualifiedWorkflowSession());

		Assert.assertEquals("service", scopeExpression.getServiceName());

		scopeExpression = new ScopeExpression("[Service:service.ND:node]");
		Assert.assertTrue(scopeExpression.matches());
		Assert.assertFalse(scopeExpression.hasFolderSession());
		Assert.assertFalse(scopeExpression.hasGlobal());
		Assert.assertFalse(scopeExpression.hasSession());
		Assert.assertTrue(scopeExpression.hasIntegrationService());
		Assert.assertTrue(scopeExpression.hasNodeName());
		Assert.assertFalse(scopeExpression.hasQualifiedWorkflowSession());

		Assert.assertEquals("service", scopeExpression.getServiceName());
		Assert.assertEquals("node", scopeExpression.getNodeName());
	}

	@Test
	public void testQualifiedWorkflowScope()
	{
		ScopeExpression scopeExpression = new ScopeExpression("[folder.WF:workflow]");
		Assert.assertTrue(scopeExpression.matches());
		Assert.assertFalse(scopeExpression.hasFolderSession());
		Assert.assertFalse(scopeExpression.hasGlobal());
		Assert.assertFalse(scopeExpression.hasSession());
		Assert.assertFalse(scopeExpression.hasIntegrationService());
		Assert.assertFalse(scopeExpression.hasNodeName());
		Assert.assertTrue(scopeExpression.hasQualifiedWorkflowSession());

		Assert.assertEquals("folder", scopeExpression.getQualifiedFolderName());
		Assert.assertEquals("workflow", scopeExpression.getWorkflowName());
		Assert.assertFalse(scopeExpression.hasWorkletName());
		Assert.assertFalse(scopeExpression.hasSession());

		scopeExpression = new ScopeExpression("[folder.WF:workflow.WT:worklet]");
		Assert.assertTrue(scopeExpression.matches());
		Assert.assertFalse(scopeExpression.hasFolderSession());
		Assert.assertFalse(scopeExpression.hasGlobal());
		Assert.assertFalse(scopeExpression.hasSession());
		Assert.assertFalse(scopeExpression.hasIntegrationService());
		Assert.assertFalse(scopeExpression.hasNodeName());
		Assert.assertTrue(scopeExpression.hasQualifiedWorkflowSession());

		Assert.assertEquals("folder", scopeExpression.getQualifiedFolderName());
		Assert.assertEquals("workflow", scopeExpression.getWorkflowName());
		Assert.assertEquals("worklet", scopeExpression.getWorkletName());
		Assert.assertFalse(scopeExpression.hasSession());

		scopeExpression = new ScopeExpression("[folder.WF:workflow.WT:worklet.ST:session]");
		Assert.assertTrue(scopeExpression.matches());
		Assert.assertFalse(scopeExpression.hasFolderSession());
		Assert.assertFalse(scopeExpression.hasGlobal());
		Assert.assertFalse(scopeExpression.hasSession());
		Assert.assertFalse(scopeExpression.hasIntegrationService());
		Assert.assertFalse(scopeExpression.hasNodeName());
		Assert.assertTrue(scopeExpression.hasQualifiedWorkflowSession());

		Assert.assertEquals("folder", scopeExpression.getQualifiedFolderName());
		Assert.assertEquals("workflow", scopeExpression.getWorkflowName());
		Assert.assertEquals("worklet", scopeExpression.getWorkletName());
		Assert.assertEquals("session", scopeExpression.getSessionName());
	}

	@Test
	public void testParameters()
	{
		ParameterExpression parameterExpression = new ParameterExpression("$A=B");
		Assert.assertTrue(parameterExpression.matches());
		Assert.assertFalse(parameterExpression.hasMapplet());
		Assert.assertEquals("$A", parameterExpression.getParameter());
		Assert.assertEquals("B", parameterExpression.getValue());

		parameterExpression = new ParameterExpression("mplt.$A=B");
		Assert.assertTrue(parameterExpression.matches());
		Assert.assertTrue(parameterExpression.hasMapplet());
		Assert.assertEquals("mplt", parameterExpression.getMapplet());
		Assert.assertEquals("$A", parameterExpression.getParameter());
		Assert.assertEquals("B", parameterExpression.getValue());
	}
}