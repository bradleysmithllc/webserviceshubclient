package org.bitbucket.bradleysmithllc.webserviceshubclient;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import com.informatica.wsh.*;
import org.bitbucket.bradleysmithllc.java_cl_parser.CapturingShutdownHandler;
import org.bitbucket.bradleysmithllc.java_cl_parser.ShutdownHandler;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

public class JCmdIntegrationTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	private CapturingShutdownHandler capturingShutdownHandler;

	@Before
	public void setupShutdownHandler()
	{
		capturingShutdownHandler = new CapturingShutdownHandler();
		ShutdownHandler.setInstance(capturingShutdownHandler);
	}

	//@Test
	public void test() throws Exception
	{
		DataIntegrationService dis = new DataIntegrationService();

		SessionIdHandlerResolver sessionIdHandlerResolver = new SessionIdHandlerResolver();
		dis.setHandlerResolver(sessionIdHandlerResolver);

		DataIntegrationInterface iface = dis.getDataIntegration();

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setRepositoryDomainName("DOM_DEV");
		loginRequest.setRepositoryName("REP_SVC_DEV");
		loginRequest.setUserName("apaparaju");
		loginRequest.setPassword("apaparaju");

		String login = iface.login(loginRequest);

		sessionIdHandlerResolver.setSessionId(login);

		System.out.println(login);

		PingDIServerRequest pingDIServer = new PingDIServerRequest();
		DIServiceInfo diServiceInfo = new DIServiceInfo();
		diServiceInfo.setDomainName("DOM_DEV");
		diServiceInfo.setServiceName("INT_SVC_DEV");

		pingDIServer.setDIServiceInfo(diServiceInfo);
		pingDIServer.setTimeOut(1000);

		EPingState res = iface.pingDIServer(pingDIServer);
		System.out.println(res.name());

		try
		{
			TypeStartWorkflowExRequest workflowRequest = new TypeStartWorkflowExRequest();
			workflowRequest.setDIServiceInfo(diServiceInfo);
			workflowRequest.setFolderName("REPOSITORY_UTILITIES");
			workflowRequest.setWorkflowName("wkf_SET_TO_HIGHER_SEQUENCE_VALUES");
			workflowRequest.setRequestMode(ETaskRunMode.NORMAL);

			String config = IOUtils.toString(
				new URL("http://config.sonicdrivein.com/settings/dev/INFA_" + workflowRequest.getFolderName() + "_" + workflowRequest.getWorkflowName() + ".properties")
			);

			Properties props = new Properties();
			props.load(new StringReader(config));

			ParameterArray parameterArray = new ParameterArray();

			for (Map.Entry<Object, Object> prop : props.entrySet())
			{
				Parameter parameter = new Parameter();
				parameter.setName(String.valueOf(prop.getKey()));
				parameter.setValue(String.valueOf(prop.getValue()));
				parameter.setScope("GLOBAL");
				parameterArray.getParameters().add(parameter);
			}


			workflowRequest.setParameters(parameterArray);

			TypeStartWorkflowExResponse resp = iface.startWorkflowEx(workflowRequest);

			System.out.println(resp.getRunId());

			while (true)
			{
				TypeGetWorkflowDetailsExRequest getWorkflowDetailsEx = new TypeGetWorkflowDetailsExRequest();
				getWorkflowDetailsEx.setDIServiceInfo(diServiceInfo);
				getWorkflowDetailsEx.setFolderName(workflowRequest.getFolderName());
				getWorkflowDetailsEx.setWorkflowName(workflowRequest.getWorkflowName());
				getWorkflowDetailsEx.setWorkflowRunId(resp.getRunId());

				DIServerDetails status = iface.getWorkflowDetailsEx(getWorkflowDetailsEx);

				System.out.println(status.getWorkflowDetails().get(0).getWorkflowRunStatus().name());

				if (status.getWorkflowDetails().get(0).getWorkflowRunStatus() != EWorkflowRunStatus.RUNNING)
				{
					break;
				}

				Thread.sleep(300L);
			}
		}
		catch (Fault fau)
		{
			System.out.println(fau.getMessage());
			System.out.println(fau.getFaultInfo().getExtendedDetails());
		}

		iface.logout(new VoidRequest());
	}

	//@Test
	public void testClientDev() throws IOException
	{
		// create the parameter file
		File parm = temporaryFolder.newFile("no_data.prm");

		FileUtils.write(parm, "[Global]");

		StartWorkflow.main(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-f",
				"_BSMITH",
				"-wkf",
				"wkf_LOAD_NO_DATA",
				"-prm",
				parm.getAbsolutePath(),
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL"
		});
	}

	@Test
	public void testClientDevTask() throws IOException
	{
		// create the parameter file
		File parm = temporaryFolder.newFile("no_data.prm");

		FileUtils.write(parm, "[Global]\n$PMSessionLogFile=Doogie.txt$$SLEEP_FG=N");

		StartWorkflow.main(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-rin",
				"WHATEVER",
				"-dbg",
				"-f",
				"REPOSITORY_UTILITIES",
				"-wkf",
				"wkf_LOAD_NO_DATA",
				"-dbg",
				"-prm",
				parm.getAbsolutePath(),
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL"
		});
	}

	@Test
	public void none()
	{
	}
}