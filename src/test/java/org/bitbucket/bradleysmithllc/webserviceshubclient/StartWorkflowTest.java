package org.bitbucket.bradleysmithllc.webserviceshubclient;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.informatica.wsh.*;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.java_cl_parser.*;
import org.bitbucket.bradleysmithllc.webserviceshubclient.impl.MockWebServicesHub;
import org.bitbucket.bradleysmithllc.webserviceshubclient.impl.WebServicesHub;
import org.bitbucket.bradleysmithllc.webserviceshubclient.impl.WebServicesHubFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class StartWorkflowTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	private CapturingShutdownHandler capturingShutdownHandler;
	private MockWebServicesHub mockWebServicesHub;

	@Before
	public void setupShutdownHandler()
	{
		capturingShutdownHandler = new CapturingShutdownHandler();
		ShutdownHandler.setInstance(capturingShutdownHandler);
		mockWebServicesHub = new MockWebServicesHub();
		WebServicesHubFactory.setWebServicesHubInstance(mockWebServicesHub);
		mockWebServicesHub.clearParameters();
	}

	@Test
	public void testClientDevTask() throws IOException
	{
		mockWebServicesHub.addParameter("domainName", "DOM_DEV")
			.addParameter("integrationServiceName", "INT_SVC_DEV")
			.addParameter("repositoryName", "REP_SVC_DEV")
			.addParameter("wsdlURL", "http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL")
			.addParameter("securityDomain", "SONIC_LDAP")
			.addParameter("integrationServiceTimeout", String.valueOf("1000"))
			.addParameter("userName", "apaparaju")
			.addParameter("password", "apaparaju")
			.addParameter("folderName", "_BSMITH")
			.addParameter("workflowName", "wkf_LOAD_NO_DATA")
			.addParameter("taskPath", "path")
			.addParameter("runid", "WHATEVER");

		// create the parameter file
		File parm = temporaryFolder.newFile("no_data.prm");

		FileUtils.write(parm, "[Global]\n$PMSessionLogFile=Doogie.txt");

		StartWorkflow.main(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-rin",
				"WHATEVER",
				"-dbg",
				"-f",
				"_BSMITH",
				"-wkf",
				"wkf_LOAD_NO_DATA",
				"-tip",
				"path",
				"-dbg",
				"-prm",
				parm.getAbsolutePath(),
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL",
				"-sd",
				"SONIC_LDAP",
				"-isvc",
				"1000"
		});

		Assert.assertNull(capturingShutdownHandler.getExitCode());
	}

	@Test(expected = IOException.class)
	public void testFailedExecution() throws Exception
	{
		mockWebServicesHub.addAction(new MockWebServicesHub.Action(WebServicesHub.workflowStatus.FAILED));
		// create the parameter file
		File parm = temporaryFolder.newFile("no_data.prm");

		FileUtils.write(parm, "[Global]\n$PMSessionLogFile=Doogie.txt");

		StartWorkflow.mainWithExceptions(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-rin",
				"WHATEVER",
				"-dbg",
				"-f",
				"_BSMITH",
				"-wkf",
				"wkf_LOAD_NO_DATA",
				"-tip",
				"path",
				"-dbg",
				"-prm",
				parm.getAbsolutePath(),
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL",
				"-sd",
				"SONIC_LDAP",
				"-isvc",
				"1000"
		});
	}

	@Test(expected = IOException.class)
	public void tooManyFaults() throws Exception
	{
		mockWebServicesHub.addAction(
			new MockWebServicesHub.Action(new Fault("", null))
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		);

		// create the parameter file
		File parm = temporaryFolder.newFile("no_data.prm");

		FileUtils.write(parm, "[Global]\n$PMSessionLogFile=Doogie.txt");

		StartWorkflow.mainWithExceptions(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-rin",
				"WHATEVER",
				"-dbg",
				"-f",
				"_BSMITH",
				"-wkf",
				"wkf_LOAD_NO_DATA",
				"-tip",
				"path",
				"-dbg",
				"-prm",
				parm.getAbsolutePath(),
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL",
				"-sd",
				"SONIC_LDAP",
				"-isvc",
				"1000"
		});
	}

    @Test
	public void retryCountIsSetFromCLIOption() throws Exception
	{
		mockWebServicesHub.addAction(
			new MockWebServicesHub.Action(new Fault("", null)) //1
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null)) //2
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null)) //3
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null)) //4
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null)) //5
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null)) //6
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null)) //7
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null)) //8
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null)) //9
		)
		.addAction(
				new MockWebServicesHub.Action(new Fault("", null)) //10
		);

		// create the parameter file
		File parm = temporaryFolder.newFile("no_data.prm");

		FileUtils.write(parm, "[Global]\n$PMSessionLogFile=Doogie.txt");

		StartWorkflow.mainWithExceptions(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-rin",
				"WHATEVER",
				"-dbg",
				"-f",
				"_BSMITH",
				"-wkf",
				"wkf_LOAD_NO_DATA",
				"-tip",
				"path",
				"-dbg",
				"-prm",
				parm.getAbsolutePath(),
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL",
				"-sd",
				"SONIC_LDAP",
				"-isvc",
				"1000",
                "-retryCount",
                "11"
		});
	}

	@Test
	public void tooManyFaultsInterspersed() throws Exception
	{
		mockWebServicesHub.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(WebServicesHub.workflowStatus.RUNNING)
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				);

		// create the parameter file
		File parm = temporaryFolder.newFile("no_data.prm");

		FileUtils.write(parm, "[Global]\n$PMSessionLogFile=Doogie.txt");

		StartWorkflow.mainWithExceptions(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-rin",
				"WHATEVER",
				"-dbg",
				"-f",
				"_BSMITH",
				"-wkf",
				"wkf_LOAD_NO_DATA",
				"-tip",
				"path",
				"-dbg",
				"-prm",
				parm.getAbsolutePath(),
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL",
				"-sd",
				"SONIC_LDAP",
				"-isvc",
				"1000"
		});
	}

	@Test(expected = IOException.class)
	public void tooManyFaultsDie() throws Exception
	{
		mockWebServicesHub.addAction(
				new MockWebServicesHub.Action(new Fault("", null))
		)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(WebServicesHub.workflowStatus.RUNNING)
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(WebServicesHub.workflowStatus.RUNNING)
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(WebServicesHub.workflowStatus.RUNNING)
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(WebServicesHub.workflowStatus.RUNNING)
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new Fault("", null))
				);

		// create the parameter file
		File parm = temporaryFolder.newFile("no_data.prm");

		FileUtils.write(parm, "[Global]\n$PMSessionLogFile=Doogie.txt");

		StartWorkflow.mainWithExceptions(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-rin",
				"WHATEVER",
				"-dbg",
				"-f",
				"_BSMITH",
				"-wkf",
				"wkf_LOAD_NO_DATA",
				"-tip",
				"path",
				"-dbg",
				"-prm",
				parm.getAbsolutePath(),
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL",
				"-sd",
				"SONIC_LDAP",
				"-isvc",
				"1000"
		});
	}

	@Test(expected = IOException.class)
	public void tooManyRuntimeExceptions() throws Exception
	{
		mockWebServicesHub.addAction(
				new MockWebServicesHub.Action(new RuntimeException("", null))
		)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				)
				.addAction(
						new MockWebServicesHub.Action(new RuntimeException("", null))
				);

		// create the parameter file
		File parm = temporaryFolder.newFile("no_data.prm");

		FileUtils.write(parm, "[Global]\n$PMSessionLogFile=Doogie.txt");

		StartWorkflow.mainWithExceptions(new String[]{
				"-dn",
				"DOM_DEV",
				"-rn",
				"REP_SVC_DEV",
				"-isvc",
				"INT_SVC_DEV",
				"-un",
				"apaparaju",
				"-pwd",
				"apaparaju",
				"-rin",
				"WHATEVER",
				"-dbg",
				"-f",
				"_BSMITH",
				"-wkf",
				"wkf_LOAD_NO_DATA",
				"-tip",
				"path",
				"-dbg",
				"-prm",
				parm.getAbsolutePath(),
				"-wsdl",
				"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL",
				"-sd",
				"SONIC_LDAP",
				"-isvc",
				"1000"
		});
	}

	@Test
	public void sleepTime() throws UsageException, InvalidCLIEntryException, InvocationTargetException {
		StartWorkflow sw = new StartWorkflow();

		CommonsCLILauncher.mainWithInstance(sw, new String []{
						"-dn",
						"DOM_DEV",
						"-rn",
						"REP_SVC_DEV",
						"-isvc",
						"INT_SVC_DEV",
						"-un",
						"apaparaju",
						"-pwd",
						"apaparaju",
						"-rin",
						"WHATEVER",
						"-dbg",
						"-f",
						"_BSMITH",
						"-wkf",
						"wkf_LOAD_NO_DATA",
						"-tip",
						"path",
						"-dbg",
						"-prm",
						"ll",
						"-wsdl",
						"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL",
						"-sd",
						"SONIC_LDAP",
						"-isvc",
						"1000",
						"-sp",
						"7"
				}
		);

		Assert.assertEquals(sw.getSleepPeriod(), 7);
	}

	@Test
	public void retryCount() throws UsageException, InvalidCLIEntryException, InvocationTargetException {
		StartWorkflow sw = new StartWorkflow();

		CommonsCLILauncher.mainWithInstance(sw, new String []{
						"-dn",
						"DOM_DEV",
						"-rn",
						"REP_SVC_DEV",
						"-isvc",
						"INT_SVC_DEV",
						"-un",
						"apaparaju",
						"-pwd",
						"apaparaju",
						"-rin",
						"WHATEVER",
						"-dbg",
						"-f",
						"_BSMITH",
						"-wkf",
						"wkf_LOAD_NO_DATA",
						"-tip",
						"path",
						"-dbg",
						"-prm",
						"ll",
						"-wsdl",
						"http://etldev02:7333/wsh/services/BatchServices/DataIntegration?WSDL",
						"-sd",
						"SONIC_LDAP",
						"-isvc",
						"1000",
						"-retryCount",
						"773"
				}
		);

		Assert.assertEquals(sw.getRetryCount(), 773);
	}
}