package org.bitbucket.bradleysmithllc.webserviceshubclient.impl;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.informatica.wsh.Fault;

import java.io.File;
import java.io.IOException;

public interface WebServicesHub {
	/**
	 *	Start a workflow
	 */
	void startWorkflow(
			String domainName,
			String integrationServiceName,
			String repositoryName,
			String wsdlURL,
			String securityDomain,
			int integrationServiceTimeout,
			String userName,
			String password,
			String folderName,
			String workflowName,
			String taskPath,
			String runid,
			File prm
	) throws IOException, Fault;

	enum workflowStatus {
		SUCCEEDED,
		FAILED,
		RUNNING
	}

	/**
	 * Return the status of the last workflow started by this object
	 */
	workflowStatus getWorkflowStatus() throws Fault;

	/**
	 * Introduces a pause.
	 */
	void sleep(int seconds);
}