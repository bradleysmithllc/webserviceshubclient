package org.bitbucket.bradleysmithllc.webserviceshubclient.impl;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.informatica.wsh.*;
import org.bitbucket.bradleysmithllc.webserviceshubclient.SessionIdHandlerResolver;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ScopeSection;

import javax.xml.namespace.QName;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;

public class InformaticaWebServicesHub implements WebServicesHub {
	private boolean debug = false;
	private LoginRequest loginRequest;
	private DIServiceInfo diServiceInfo;
	private SessionIdHandlerResolver sessionIdHandlerResolver;
	private DataIntegrationService dis;
	private DataIntegrationInterface iface;
	private TypeStartWorkflowExRequest workflowRequest;
	private int runningWorkflowIdentifier;

	@Override
	public void startWorkflow(
			String domainName,
			String integrationServiceName,
			String repositoryName,
			String wsdlURL,
			String securityDomain,
			int integrationServiceTimeout,
			String userName,
			String password,
			String folderName,
			String workflowName,
			String taskPath,
			String runid,
			File prm
	) throws IOException, Fault {
		dis = new DataIntegrationService(
				new URL(wsdlURL),
				new QName("http://www.informatica.com/wsh", "DataIntegrationService")
		);

		diServiceInfo = new DIServiceInfo();
		diServiceInfo.setDomainName(domainName);
		diServiceInfo.setServiceName(integrationServiceName);

		sessionIdHandlerResolver = new SessionIdHandlerResolver();
		dis.setHandlerResolver(sessionIdHandlerResolver);

		iface = dis.getDataIntegration();

		loginRequest = new LoginRequest();
		loginRequest.setRepositoryDomainName(domainName);
		loginRequest.setRepositoryName(repositoryName);
		loginRequest.setUserName(userName);
		loginRequest.setPassword(password);

		workflowRequest = new TypeStartWorkflowExRequest();
		workflowRequest.setDIServiceInfo(diServiceInfo);
		workflowRequest.setFolderName(folderName);
		workflowRequest.setWorkflowName(workflowName);

		if (taskPath != null) {
			workflowRequest.setTaskInstancePath(taskPath);
		}

		if (runid != null) {
			workflowRequest.setWorkflowRunInstanceName(runid);
		}

		workflowRequest.setRequestMode(ETaskRunMode.NORMAL);

		if (prm != null) {
			ParameterArray parameterArray = new ParameterArray();

			workflowRequest.setParameters(parameterArray);

			ParameterFile pf = new ParameterFile(prm);

			if (debug) {
				System.out.println("Binding parameters from file: " + prm);
			}

			for (ScopeSection section : pf.getSections()) {
				if (debug) {
					System.out.println("Binding parameters from section: " + section.getHeader() + " - literally '" + section.getWSHeader() + "'");
				}

				for (org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.Parameter parameter : section.getParameters()) {
					if (debug) {
						System.out.println("Binding parameter: " + parameter.asString());
					}

					com.informatica.wsh.Parameter wsparameter = new com.informatica.wsh.Parameter();
					wsparameter.setName(parameter.getQualifiedParameterName());
					wsparameter.setValue(parameter.getParameterValue());
					wsparameter.setScope(section.getWSHeader());
					parameterArray.getParameters().add(wsparameter);
				}
			}
		}

		if (securityDomain != null) {
			loginRequest.setUserNameSpace(securityDomain);
		}

		String sessionId = iface.login(loginRequest);

		try {
			sessionIdHandlerResolver.setSessionId(sessionId);

			PingDIServerRequest pingDIServer = new PingDIServerRequest();

			pingDIServer.setDIServiceInfo(diServiceInfo);
			pingDIServer.setTimeOut(integrationServiceTimeout);

			EPingState res = iface.pingDIServer(pingDIServer);

			if (res != EPingState.ALIVE) {
				throw new IOException("Integration Service ping timeout");
			}

			TypeStartWorkflowExResponse resp = iface.startWorkflowEx(workflowRequest);
			runningWorkflowIdentifier = resp.getRunId();

			System.out.println("Running with run identifier: " + runningWorkflowIdentifier);
			System.out.println();
		} finally {
			iface.logout(new VoidRequest());
		}
	}

	@Override
	public workflowStatus getWorkflowStatus() throws Fault {
		try {
			String sessionId = iface.login(loginRequest);

			sessionIdHandlerResolver.setSessionId(sessionId);
			TypeGetWorkflowDetailsExRequest getWorkflowDetailsEx = new TypeGetWorkflowDetailsExRequest();
			getWorkflowDetailsEx.setDIServiceInfo(diServiceInfo);
			getWorkflowDetailsEx.setFolderName(workflowRequest.getFolderName());
			getWorkflowDetailsEx.setWorkflowName(workflowRequest.getWorkflowName());
			getWorkflowDetailsEx.setWorkflowRunId(runningWorkflowIdentifier);

			DIServerDetails status = iface.getWorkflowDetailsEx(getWorkflowDetailsEx);

			System.out.println(new Timestamp(System.currentTimeMillis()));
			EWorkflowRunStatus workflowRunStatus = status.getWorkflowDetails().get(0).getWorkflowRunStatus();
			System.out.println("Workflow status: " + workflowRunStatus.name());
			System.out.println();

			switch (workflowRunStatus)
			{
				case SUCCEEDED:
					return workflowStatus.valueOf(workflowRunStatus.name());

				case FAILED:
					return workflowStatus.valueOf(workflowRunStatus.name());

				case STOPPED:
				case ABORTED:
				case SUSPENDED:
				case SCHEDULED:
				case UNSCHEDULED:
				case UNKNOWN:
				case TERMINATED:
					return workflowStatus.FAILED;

				case RUNNING:
				default:
					return workflowStatus.RUNNING;
			}
		} finally {
			iface.logout(new VoidRequest());
		}
	}

	@Override
	public void sleep(int seconds) {
		try {
			Thread.sleep(seconds * 1000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
