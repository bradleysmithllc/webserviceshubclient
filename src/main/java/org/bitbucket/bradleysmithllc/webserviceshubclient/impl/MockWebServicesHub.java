package org.bitbucket.bradleysmithllc.webserviceshubclient.impl;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.informatica.wsh.Fault;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Class used for unit testing
 */
public class MockWebServicesHub implements WebServicesHub {
	private Map<String, String> workflowParameters = new HashMap<String, String>();

	public static class Action
	{
		public Action(workflowStatus wkfStatus) {
			this.wkfStatus = wkfStatus;
		}

		public Action(Fault fault) {
			this.fault = fault;
		}

		public Action(RuntimeException runtimeException) {
			this.runtimeException = runtimeException;
		}

		Fault fault;
		RuntimeException runtimeException;
		workflowStatus wkfStatus;
	}

	private Queue<Action> actionQueue = new LinkedList<Action>();

	public MockWebServicesHub addAction(Action a)
	{
		actionQueue.add(a);
		return this;
	}

	public MockWebServicesHub clearParameters()
	{
		workflowParameters.clear();
		return this;
	}

	public MockWebServicesHub addParameter(String name, String value)
	{
		workflowParameters.put(name, value);
		return this;
	}

	@Override
	public void startWorkflow(
			String domainName,
			String integrationServiceName,
			String repositoryName,
			String wsdlURL,
			String securityDomain,
			int integrationServiceTimeout,
			String userName,
			String password,
			String folderName,
			String workflowName,
			String taskPath,
			String runid,
			File prm
	) throws IOException, Fault {
		assertEquals("domainName", domainName);
		assertEquals("integrationServiceName", integrationServiceName);
		assertEquals("repositoryName", repositoryName);
		assertEquals("wsdlURL", wsdlURL);
		assertEquals("securityDomain", securityDomain);
		assertEquals("integrationServiceTimeout", String.valueOf(integrationServiceTimeout));
		assertEquals("userName", userName);
		assertEquals("password", password);
		assertEquals("folderName", folderName);
		assertEquals("workflowName", workflowName);
		assertEquals("taskPath", taskPath);
		assertEquals("runid", runid);
	}

	private void assertEquals(String domainNameProperty, String actualPropertyValue) {
		if (!workflowParameters.isEmpty())
		{
			String expectedValue = workflowParameters.get(domainNameProperty);
			if (!expectedValue.equals(actualPropertyValue))
			{
				throw new AssertionError("Property {" + domainNameProperty + "} - expected [" + expectedValue + "] but was [" + actualPropertyValue + "]");
			}
		}
	}

	@Override
	public workflowStatus getWorkflowStatus() throws Fault {
		try
		{
			Action a = actionQueue.remove();

			if (a.wkfStatus != null)
			{
				return a.wkfStatus;
			}
			else if(a.fault != null)
			{
				throw a.fault;
			}
			else if(a.runtimeException != null)
			{
				throw a.runtimeException;
			}
			else
			{
				throw new RuntimeException("Bad Script");
			}
		}
		catch(NoSuchElementException nsee)
		{
			return workflowStatus.SUCCEEDED;
		}
	}

	@Override
	public void sleep(int seconds) {
	}
}