package org.bitbucket.bradleysmithllc.webserviceshubclient;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import java.util.ArrayList;
import java.util.List;

public class SessionIdHandlerResolver implements HandlerResolver
{
	private String sessionId;
	private SessionIdHandler sessionIdHandler;

	public void setSessionId(String sessionId)
	{
		this.sessionId = sessionId;
		sessionIdHandler.setSessionId(sessionId);
	}

	public List<Handler> getHandlerChain(PortInfo portInfo)
	{
		List<Handler> handlerChain = new ArrayList<Handler>();

		sessionIdHandler = new SessionIdHandler();

		handlerChain.add(sessionIdHandler);

		return handlerChain;
	}
}