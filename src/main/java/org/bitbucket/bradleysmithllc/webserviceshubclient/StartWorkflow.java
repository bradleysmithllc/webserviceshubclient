package org.bitbucket.bradleysmithllc.webserviceshubclient;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.informatica.wsh.*;
import org.bitbucket.bradleysmithllc.java_cl_parser.*;
import org.bitbucket.bradleysmithllc.webserviceshubclient.impl.WebServicesHub;
import org.bitbucket.bradleysmithllc.webserviceshubclient.impl.WebServicesHubFactory;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ScopeSection;

import javax.xml.namespace.QName;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.sql.Timestamp;

@CLIEntry(
		nickName = "startworkflow",
		description = "Executes an informatica workflow using the Informatica Web Services Hub.  The parameters for the workflow are pulled from a parameter file.",
		contact = "bradleysmithllc@gmail.com",
		version = "2.0.3",
		versionControl = "https://bradleysmithllc@bitbucket.org/bradleysmithllc/webserviceshubclient.git",
		documentationUrl = "https://bitbucket.org/bradleysmithllc/webserviceshubclient/wiki/Home"
)
public class StartWorkflow {
	private WebServicesHub webServicesHub = WebServicesHubFactory.getWebServicesHubInstance();

	private String domainName;
	private String repositoryName;
	private String integrationServiceName;
	private String userName;
	private String password;
	private String securityDomain;

	private File prm;

	private String wsdlURL;

	private int integrationServiceTimeout;

	private String folderName;
	private String workflowName;
	private String runid;

	private String taskPath;

	private boolean asynchronous;
	private int retryCount = 10;
	private int sleepPeriod = 3;

	private boolean debug;

	public int getSleepPeriod() {
		return sleepPeriod;
	}

	public int getRetryCount() {
		return retryCount;
	}

	@CLIOption(
			name = "sleepPeriod",
			longName = "sp",
			defaultValue = "5",
			description = "Amount of time to pause between service retries.",
			valueType = CLIOption.value_type.required
	)
	public void setSleepPeriod(int sp) {
		sleepPeriod = sp;
	}

	@CLIOption(
			name = "dbg",
			longName = "debug",
			description = "Debug.",
			valueType = CLIOption.value_type.not_allowed
	)
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	@CLIOption(
			name = "rin",
			longName = "run-instance",
			description = "The run instance name.  Usually required for concurrent workflows.",
			valueType = CLIOption.value_type.required
	)
	public void setRunid(String runid) {
		this.runid = runid;
	}

	@CLIOption(
			name = "retryCount",
			longName = "retry-count",
			description = "When run asynchrously, the number of times to recheck status before giving up.",
			valueType = CLIOption.value_type.required
	)
	public void setRetryCount(int count) {
		this.retryCount = count;
	}

	@CLIOption(
			name = "prm",
			longName = "parameter-file-path",
			description = "The path to the parameter file, if one is needed.  This path is relative to the caller, not the PowerCenter server.",
			valueType = CLIOption.value_type.required
	)
	public void setPrmFile(String path) {
		prm = new File(path);
	}

	@CLIOption(
			name = "sd",
			longName = "security-domain",
			description = "The name of the security domain.  If not supplied native will be used.  Passing 'native' will not work.",
			valueType = CLIOption.value_type.required
	)
	public void setSecurityDomain(String securityDomain) {
		this.securityDomain = securityDomain;
	}

	@CLIOption(
			name = "async",
			longName = "asynchronous",
			description = "If true, the command returns once the workflow is started.  Otherwise, the command blocks until the workflow completes.",
			valueType = CLIOption.value_type.not_allowed,
			defaultValue = "false"
	)
	public void setAsynchronous(boolean asynchronous) {
		this.asynchronous = asynchronous;
	}

	@CLIOption(
			name = "svcto",
			longName = "integration-service-timeout",
			description = "Integration Service ping timeout",
			valueType = CLIOption.value_type.required,
			defaultValue = "1000"
	)
	public void setIntegrationServiceTimeout(int integrationServiceTimeout) {
		this.integrationServiceTimeout = integrationServiceTimeout;
	}

	@CLIOption(
			name = "dn",
			longName = "domain-name",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The name of the Informatica Domain"
	)
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	@CLIOption(
			name = "rn",
			longName = "repository-name",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The name of the Repository Serice"
	)
	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}

	@CLIOption(
			name = "isvc",
			longName = "integration-service",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The name of the Integration Service"
	)
	public void setIntegrationServiceName(String integrationServiceName) {
		this.integrationServiceName = integrationServiceName;
	}

	@CLIOption(
			name = "un",
			longName = "user-name",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The user name"
	)
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@CLIOption(
			name = "pwd",
			longName = "password",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The password"
	)
	public void setPassword(String password) {
		this.password = password;
	}

	@CLIOption(
			name = "wsdl",
			longName = "wsdl-url",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The URL to the WSDL"
	)
	public void setWsdlURL(String wsdlURL) {
		this.wsdlURL = wsdlURL;
	}

	@CLIOption(
			name = "f",
			longName = "folder",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "Repository folder name"
	)
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	@CLIOption(
			name = "wkf",
			longName = "workflow",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The workflow name"
	)
	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	@CLIOption(
			name = "tip",
			valueType = CLIOption.value_type.required,
			longName = "task-instance-path",
			description = "The task instance path execute."
	)
	public void setTaskPath(String taskPath) {
		this.taskPath = taskPath;
	}

	@CLIMain
	public void main() throws IOException, Fault {
		webServicesHub.startWorkflow(
				domainName,
				integrationServiceName,
				repositoryName,
				wsdlURL,
				securityDomain,
				integrationServiceTimeout,
				userName,
				password,
				folderName,
				workflowName,
				taskPath,
				runid,
				prm
		);

		int faultCount = 0;

		WHILE:
		while (!asynchronous) {
			try {
				WebServicesHub.workflowStatus status = webServicesHub.getWorkflowStatus();

				faultCount = 0;

				switch (status) {
					case SUCCEEDED:
						System.out.println("Workflow execution succeeded");
						break WHILE;
					case FAILED:
						throw new IOException("Workflow execution failed");
				}
			}
			catch(Fault flt)
			{
				faultCount++;
				System.out.println("Error connecting to the Web Services Hub: " + flt.toString());
				System.out.println("This was try [" + faultCount + "] of [" + retryCount + "]");

				if (faultCount >= retryCount)
				{
					throw new IOException("Too many consecutive faults checking status [" + faultCount + "]");
				}
			}
			catch(RuntimeException flt)
			{
				faultCount++;
				System.out.println("Error connecting to the Web Services Hub: " + flt.toString());
				System.out.println("This was try [" + faultCount + "] of [" + retryCount + "]");

				if (faultCount >= retryCount)
				{
					throw new IOException("Too many consecutive faults checking status [" + faultCount + "]");
				}
			}

			webServicesHub.sleep(sleepPeriod);
		}
	}

	public static void main(String[] argv) {
		CommonsCLILauncher.mainClean(argv);
	}

	public static void mainWithExceptions(String[] argv) throws IOException, Fault {
		try {
			CommonsCLILauncher.main(argv);
		} catch (InvocationTargetException exc) {
			Throwable tar = exc.getTargetException();

			if (tar instanceof Fault) {
				throw (Fault) tar;
			} else if (tar instanceof IOException) {
				throw (IOException) tar;
			} else if (tar instanceof RuntimeException) {
				throw (RuntimeException) tar;
			}
		} catch (MissingCLIEntryException e) {
			e.printStackTrace();
		} catch (UsageException e) {
			e.printStackTrace();
		} catch (InvalidCLIEntryException e) {
			e.printStackTrace();
		}
	}
}