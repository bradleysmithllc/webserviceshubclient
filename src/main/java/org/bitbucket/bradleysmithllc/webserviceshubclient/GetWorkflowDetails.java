package org.bitbucket.bradleysmithllc.webserviceshubclient;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.informatica.wsh.*;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;

import javax.xml.namespace.QName;
import java.io.IOException;
import java.net.URL;

@CLIEntry(
		nickName = "getworkflowdetails",
		description = "Executes an informatica workflow using the Informatica Web Services Hub.  The parameters for the workflow are pulled from a parameter file.",
		contact = "bradleysmithllc@gmail.com",
		version = "2.0.3",
		versionControl = "https://bradleysmithllc@bitbucket.org/bradleysmithllc/webserviceshubclient.git",
		documentationUrl = "https://bitbucket.org/bradleysmithllc/webserviceshubclient/wiki/Home"
)
public class GetWorkflowDetails {
	private String domainName;
	private String repositoryName;
	private String integrationServiceName;
	private String userName;
	private String password;
	private String securityDomain;

	private String wsdlURL;

	private int integrationServiceTimeout;

	private String folderName;
	private String workflowName;
	private DIServerDetails workflowDetails;

	@CLIOption(
			name = "sd",
			longName = "security-domain",
			description = "The name of the security domain.  If not supplied native will be used.  Passing 'native' will not work.",
			valueType = CLIOption.value_type.required
	)
	public void setSecurityDomain(String securityDomain) {
		this.securityDomain = securityDomain;
	}

	@CLIOption(
			name = "svcto",
			longName = "integration-service-timeout",
			description = "Integration Service ping timeout",
			valueType = CLIOption.value_type.required,
			defaultValue = "1000"
	)
	public void setIntegrationServiceTimeout(int integrationServiceTimeout) {
		this.integrationServiceTimeout = integrationServiceTimeout;
	}

	@CLIOption(
			name = "dn",
			longName = "domain-name",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The name of the Informatica Domain"
	)
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	@CLIOption(
			name = "rn",
			longName = "repository-name",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The name of the Repository Serice"
	)
	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}

	@CLIOption(
			name = "isvc",
			longName = "integration-service",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The name of the Integration Service"
	)
	public void setIntegrationServiceName(String integrationServiceName) {
		this.integrationServiceName = integrationServiceName;
	}

	@CLIOption(
			name = "un",
			longName = "user-name",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The user name"
	)
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@CLIOption(
			name = "pwd",
			longName = "password",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The password"
	)
	public void setPassword(String password) {
		this.password = password;
	}

	@CLIOption(
			name = "wsdl",
			longName = "wsdl-url",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The URL to the WSDL"
	)
	public void setWsdlURL(String wsdlURL) {
		this.wsdlURL = wsdlURL;
	}

	@CLIOption(
			name = "f",
			longName = "folder",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "Repository folder name"
	)
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	@CLIOption(
			name = "wkf",
			longName = "workflow",
			valueType = CLIOption.value_type.required,
			required = true,
			description = "The workflow name"
	)
	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	@CLIMain
	public void main() throws IOException, Fault {
		try {
			DataIntegrationService dis = new DataIntegrationService(
					new URL(wsdlURL),
					new QName("http://www.informatica.com/wsh", "DataIntegrationService")
			);

			DIServiceInfo diServiceInfo = new DIServiceInfo();
			diServiceInfo.setDomainName(domainName);
			diServiceInfo.setServiceName(integrationServiceName);

			SessionIdHandlerResolver sessionIdHandlerResolver = new SessionIdHandlerResolver();
			dis.setHandlerResolver(sessionIdHandlerResolver);

			DataIntegrationInterface iface = dis.getDataIntegration();

			LoginRequest loginRequest = new LoginRequest();
			loginRequest.setRepositoryDomainName(domainName);
			loginRequest.setRepositoryName(repositoryName);
			loginRequest.setUserName(userName);
			loginRequest.setPassword(password);

			TypeGetWorkflowDetailsExRequest typeGetWorkflowDetailsExRequest = new TypeGetWorkflowDetailsExRequest();

			typeGetWorkflowDetailsExRequest.setDIServiceInfo(diServiceInfo);
			typeGetWorkflowDetailsExRequest.setFolderName(folderName);
			typeGetWorkflowDetailsExRequest.setWorkflowName(workflowName);

			if (securityDomain != null) {
				loginRequest.setUserNameSpace(securityDomain);
			}

            // DataIntegrationInterface.login throws Fault
			String sessionId = iface.login(loginRequest);

			try {
				sessionIdHandlerResolver.setSessionId(sessionId);

				PingDIServerRequest pingDIServer = new PingDIServerRequest();

				pingDIServer.setDIServiceInfo(diServiceInfo);
				pingDIServer.setTimeOut(integrationServiceTimeout);

				EPingState res = iface.pingDIServer(pingDIServer);

				if (res != EPingState.ALIVE) {
					throw new IOException("Integration Service ping timeout");
				}

				workflowDetails = iface.getWorkflowDetailsEx(typeGetWorkflowDetailsExRequest);

				System.out.println("Details: " + workflowDetails);
				System.out.println();
			} finally {
				iface.logout(new VoidRequest());
			}
		} catch (Fault fault) {
			fault.printStackTrace();
			throw fault;
		}
	}

	public DIServerDetails getWorkflowDetails() {
		return workflowDetails;
	}

	public static void main(String[] argv) {
		CommonsCLILauncher.mainClean(argv);
	}

	public static Object mainWithReturn(String[] argv) {
		return CommonsCLILauncher.mainClean(argv);
	}
}