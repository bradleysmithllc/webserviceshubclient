package org.bitbucket.bradleysmithllc.webserviceshubclient.parameter;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.bitbucket.bradleysmithllc.webserviceshubclient.regexp.ParameterExpression;

import java.io.PrintWriter;
import java.util.List;

public interface ScopeSection extends Comparable<ScopeSection>
{
	void appendParameter(ParameterExpression pe);
	void appendParameter(Parameter pe);
	void appendParameter(String name, String value);
	void appendParameter(String mapplet, String name, String value);
	void appendParameter(String line);

	void write(PrintWriter output);

	String getHeader();

	String getWSHeader();

	enum section_type
	{
		global,
		integration_service,
		integration_service_with_node,

		folder_with_workflow,
		folder_with_workflow_and_worklet,
		folder_with_workflow_and_worklet_and_session,
		folder_with_session,

		session
	}

	section_type getType();

	String getIntegrationServiceName();
	String getIntegrationServiceNodeName();

	String getFolderName();
	String getWorkflowName();
	String getWorkletName();
	String getSessionName();

	List<Parameter> getParameters();
}