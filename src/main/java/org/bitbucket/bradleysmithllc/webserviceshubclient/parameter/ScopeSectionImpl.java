package org.bitbucket.bradleysmithllc.webserviceshubclient.parameter;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.bitbucket.bradleysmithllc.webserviceshubclient.regexp.ParameterExpression;
import org.bitbucket.bradleysmithllc.webserviceshubclient.regexp.ScopeExpression;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ScopeSectionImpl implements ScopeSection
{
	private final section_type type;

	private final List<Parameter> parameters = new ArrayList<Parameter>();

	private final String folderName;
	private final String workflowName;
	private final String workletName;
	private final String sessionName;

	private final String integrationServiceName;
	private final String integrationServiceNodeName;

	public ScopeSectionImpl(ScopeExpression se)
	{
		if (se.hasSession())
		{
			type = section_type.session;
			folderName = null;
			workflowName = null;
			workletName = null;
			sessionName = se.getSession();
			integrationServiceName = null;
			integrationServiceNodeName = null;
		}
		else if (se.hasIntegrationService())
		{
			folderName = null;
			workflowName = null;
			workletName = null;
			sessionName = null;
			integrationServiceName = se.getServiceName();

			if (se.hasNodeName())
			{
				type = section_type.integration_service_with_node;
				integrationServiceNodeName = se.getNodeName();
			}
			else
			{
				type = section_type.integration_service;
				integrationServiceNodeName = null;
			}
		}
		else if (se.hasFolderSession())
		{
			type = section_type.folder_with_session;
			folderName = se.getFolderName();
			workflowName = null;
			workletName = null;
			sessionName = se.getFolderSessionName();
			integrationServiceName = null;
			integrationServiceNodeName = null;
		}
		else if (se.hasQualifiedWorkflowSession())
		{
			folderName = se.getQualifiedFolderName();
			workflowName = se.getWorkflowName();
			integrationServiceName = null;
			integrationServiceNodeName = null;

			if (se.hasWorkletName())
			{
				workletName = se.getWorkletName();

				if (se.hasSessionName())
				{
					type = section_type.folder_with_workflow_and_worklet_and_session;
					sessionName = se.getSessionName();
				}
				else
				{
					type = section_type.folder_with_workflow_and_worklet;
					sessionName = null;
				}
			}
			else
			{
				type = section_type.folder_with_workflow;
				workletName = null;
				sessionName = null;
			}
		}
		else
		{
			// global
			type = section_type.global;
			folderName = null;
			workflowName = null;
			workletName = null;
			sessionName = null;
			integrationServiceName = null;
			integrationServiceNodeName = null;
		}
	}

	public String getIntegrationServiceName()
	{
		return integrationServiceName;
	}

	public String getIntegrationServiceNodeName()
	{
		return integrationServiceNodeName;
	}

	public String getFolderName()
	{
		return folderName;
	}

	public String getWorkflowName()
	{
		return workflowName;
	}

	public String getWorkletName()
	{
		return workletName;
	}

	public String getSessionName()
	{
		return sessionName;
	}

	public void appendParameter(ParameterExpression pe)
	{
		if (!pe.hasMapplet())
		{
			appendParameter(pe.getParameter(), pe.getValue());
		}
		else
		{
			appendParameter(pe.getMapplet(), pe.getParameter(), pe.getValue());
		}
	}

	public void appendParameter(Parameter pe)
	{
		parameters.add(pe);
	}

	public void appendParameter(String name, String value)
	{
		appendParameter(null, name, value);
	}

	public void appendParameter(String mapplet, String name, String value)
	{
		parameters.add(new ParameterImpl(mapplet, name, value));
	}

	public void appendParameter(String line)
	{
		ParameterExpression pe = new ParameterExpression(line);

		if (pe.matches())
		{
			appendParameter(pe);
		}
		else
		{
			throw new IllegalArgumentException("Bad parameter: " + line);
		}
	}

	public String getHeader()
	{
		return "[" + getWSHeader() + "]";
	}

	public String getWSHeader()
	{
		StringBuilder stb = new StringBuilder();

		switch (type)
		{
			case global:
				stb.append("Global");
				break;
			case integration_service:
				stb.append("Service:").append(integrationServiceName);
				break;
			case integration_service_with_node:
				stb.append("Service:").append(integrationServiceName).append(".ND:").append(integrationServiceNodeName);
				break;
			case folder_with_workflow:
				stb.append(folderName).append(".WF:").append(workflowName);
				break;
			case folder_with_workflow_and_worklet:
				stb.append(folderName).append(".WF:").append(workflowName).append(".WT:").append(workletName);
				break;
			case folder_with_workflow_and_worklet_and_session:
				stb.append(folderName).append(".WF:").append(workflowName).append(".WT:").append(workletName).append(".ST:").append(sessionName);
				break;
			case folder_with_session:
				stb.append(folderName).append(".").append(sessionName);
				break;
			case session:
				stb.append(sessionName);
				break;
		}

		return stb.toString();
	}

	public void write(PrintWriter output)
	{
		output.println(getHeader());

		if (parameters.size() != 0)
		{
			output.println();
		}

		for (Parameter section : parameters)
		{
			section.write(output);
		}
	}

	public section_type getType()
	{
		return type;
	}

	public List<Parameter> getParameters()
	{
		return parameters;
	}

	@Override
	public String toString() {
		return "ScopeSectionImpl{" +
				"type=" + type +
				", parameters=" + parameters +
				", folderName='" + folderName + '\'' +
				", workflowName='" + workflowName + '\'' +
				", workletName='" + workletName + '\'' +
				", sessionName='" + sessionName + '\'' +
				", integrationServiceName='" + integrationServiceName + '\'' +
				", integrationServiceNodeName='" + integrationServiceNodeName + '\'' +
				'}';
	}

	@Override
	public int compareTo(ScopeSection o) {
		int sres = getWSHeader().compareTo(o.getWSHeader());

		if (sres != 0)
		{
			return sres;
		}

		List<Parameter> rparameters = o.getParameters();

		if (parameters.size() < rparameters.size())
		{
			return -1;
		}
		else if (parameters.size() > rparameters.size())
		{
			return 1;
		}
		else
		{
			for (int idx = 0; idx < parameters.size(); idx++)
			{
				Parameter lparam = parameters.get(idx);
				Parameter rparam = rparameters.get(idx);

				int pres = lparam.compareTo(rparam);

				if (pres != 0)
				{
					return pres;
				}
			}
		}

		return 0;
	}
}
