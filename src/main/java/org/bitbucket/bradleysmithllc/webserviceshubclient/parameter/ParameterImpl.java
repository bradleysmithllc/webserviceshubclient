package org.bitbucket.bradleysmithllc.webserviceshubclient.parameter;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.PrintWriter;

public class ParameterImpl implements Parameter
{
	private final String parameterName;
	private final String parameterValue;
	private final String mappletName;

	public ParameterImpl(String mappletName, String parameterName, String parameterValue)
	{
		this.parameterName = parameterName;
		this.parameterValue = parameterValue;
		this.mappletName = mappletName;
	}

	public String getParameterValue()
	{
		return parameterValue;
	}

	@Override
	public String getQualifiedParameterName() {
		return getMappletName() == null ? getParameterName() : ((getMappletName() + ".") + getParameterName());
	}

	public String getParameterName()
	{
		return parameterName;
	}

	public String getMappletName()
	{
		return mappletName;
	}

	public void write(PrintWriter output)
	{
		output.println(asString());
	}

	public String asString()
	{
		return new StringBuilder().append(getQualifiedParameterName()).append("=").append(getParameterValue()).toString();
	}

	@Override
	public String toString() {
		return "ParameterImpl{" +
				"parameterName='" + parameterName + '\'' +
				", parameterValue='" + parameterValue + '\'' +
				", mappletName=" + (mappletName == null ? "null" : ("'" + mappletName + "'"))  +
				'}';
	}

	@Override
	public int compareTo(Parameter o) {
		String oMappletName = o.getMappletName();
		String mappletName = getMappletName();

		if (oMappletName != null && mappletName != null)
		{
			int mres = oMappletName.compareTo(mappletName);

			if (mres != 0)
			{
				return mres;
			}
		}
		else if (mappletName != null)
		{
			return -1;
		}
		else if (oMappletName != null)
		{
			return 1;
		}

			int pres = parameterName.compareTo(o.getParameterName());

		if (pres == 0)
		{
			return parameterValue.compareTo(o.getParameterValue());
		}

		return pres;
	}
}