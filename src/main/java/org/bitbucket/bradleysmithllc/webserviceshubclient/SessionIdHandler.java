package org.bitbucket.bradleysmithllc.webserviceshubclient;

/*
 * #%L
 * webserviceshubclient
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.Set;

public class SessionIdHandler implements SOAPHandler
{
	private String sessionId;

	public boolean handleMessage(MessageContext context)
	{
		SOAPMessageContext smc = (SOAPMessageContext) context;

		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (outboundProperty.booleanValue() && sessionId != null)
		{
			SOAPMessage message = smc.getMessage();

			try
			{
				SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();

				SOAPHeader header = envelope.getHeader();

				if (header == null)
				{
					header = envelope.addHeader();
				}

				String WSSE_NS = "http://www.informatica.com/wsh/";
				String WSSE_PREFIX = "infa";

				SOAPElement scontext = header.addChildElement("Context", WSSE_PREFIX, WSSE_NS);
				SOAPElement sessionElemeent = scontext.addChildElement("SessionId");
				sessionElemeent.addTextNode(sessionId);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}

		return outboundProperty;
	}

	public Set<QName> getHeaders()
	{
		return null;
	}

	public boolean handleFault(MessageContext context)
	{
		return false;
	}

	public void close(MessageContext context)
	{
	}

	public void setSessionId(String sessionId)
	{
		this.sessionId = sessionId;
	}
}